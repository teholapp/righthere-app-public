# righthere-app-public

Righthere mobile application for iOS and Android.
https://www.righthere.fi

## Description
This is Righthere calendar reservation mobile application public project.
At this phase the application code is hided inside to the static library.
The plan is to open source more components at the future.

## Installation

Required dependencies:
Qt6.3 Open Source for Android, iOS and Qt5 Compatibility API.
Download from qt.io using the online installer.

Fork of QtFirebase module from https://github.com/teho1/QtFirebase.

The steps to build the application:
1) Copy or link the QtFirebase folder to the publicLibs
2) Modify publicLibs.pri and change the paths according your SDK
3) Open project in Qt creator an build your the mobile app

## Usage
Deploy the application to your device.

## Support
Contact teholapp@gmail.com

## License
LGPL V3
