# Make these modules of QtFirebase
# NOTE QTFIREBASE_SDK_PATH can be symlinked to match $$PWD/firebase_cpp_sdk
QTFIREBASE_SDK_PATH = /Users/teholapp/development/righthere-app/extensions/firebase_cpp_sdk
QTFIREBASE_FRAMEWORKS_ROOT = /Users/teholapp/development/righthere-app/extensions/ios/Firebase
QTFIREBASE_CONFIG += auth messaging
# Disable fixes for know problems
#DEFINES += QTFIREBASE_DISABLE_FIX_ANDROID_AUTO_APP_STATE_VISIBILTY
DEFINES += QTFIREBASE_BUILD_ANALYTICS QTFIREBASE_BUILD_MESSAGING
# Includes QtFirebase:
include($$PWD/QtFirebase/qtfirebase.pri)
android: include(/Users/teholapp/Library/Android/sdk/android_openssl/openssl.pri)
