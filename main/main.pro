#CUSTOM PARAMETERS
##TARGET NAME.
#USE SAME NAME FOR PROJECT MAIN FOLDER
#USE SAME NAME FOR deployProject.pro APPTARGET
#USE SAME NAME FOR main.pro TARGET
exists($$PWD/config.pri) {
    include($$PWD/config.pri)
} else{
    exists($$PWD/../config.pri) {
        include($$PWD/../config.pri)
    }
}

#TARGET = $$APPTARGET
#END CUSTOM PARAMETERS
win32{
}else{
unix{
}else{
#WEBASSEMBLY
QMAKE_LFLAGS += '-s TOTAL_MEMORY=1500mb -s TOTAL_STACK=1200mb'
}
}

CONFIG += c++14
#MANDATORY
TEMPLATE = app
SOURCES += main.cpp

RESOURCES += main.qrc # project dependencies

#OPTIONAL: ADD RELEASE.zip project library in resources
contains(ADDZIPTORESOURCES, 'yes'):{
  contains(GENERATEZIP, 'yes'):{
    exists($$PWD/release.zip){
      message("ADDING RELEASE.ZIP TO RESOURCES")
      RESOURCES +=  builder.qrc
    }
  }
}

DISTFILES += builder.qrc \ # DIST FILES ONLY
    platforms/android/res/values/colors.xml





#LINK STATIC LIB FOR PRIVATE PROJECT
#add Qt modules used in yout static library
exists($$PWD/privateProjectQtModules.pri) {
    include($$PWD/privateProjectQtModules.pri)
} else{
    exists($$PWD/../privateProject/privateProjectQtModules.pri) {
        include($$PWD/../privateProject/privateProjectQtModules.pri)
    }else{
        message("ERROR: privateProjectQtModules.pri NOT FOUND")
    }
}

#link static library
win32:LIBS += -L$$PWD/  # main distributable release
win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../privateProject/release/ -lprivateProject
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../privateProject/debug/ -lprivateProject

unix{
        android{
            DEFINES += ANDROID_STL=c++_shared
            QMAKE_CXXFLAGS += -fexceptions -frtti
            LIBS += -L$$OUT_PWD/../privateProject/ -L$$PWD/  -lprivateProject_$${QT_ARCH} # fichero copiado en la carpeta de deploy
        }else{
            LIBPRIVATE=$$OUT_PWD/../privateProject/libprivateProject.a
            !exists($$OUT_PWD/../privateProject/libprivateProject.a ) {
                    message( "Configuring local libprivateProject.a" )
                    exists($$PWD/libprivateProject.a ){
                        LIBPRIVATE= $$PWD/libprivateProject.a
                    }
             }
            LIBS += $$LIBPRIVATE
            PRE_TARGETDEPS += $$LIBPRIVATE
        }
}
exists($$PWD/../privateProject){
 INCLUDEPATH += $$PWD/../privateProject
 DEPENDPATH += $$PWD/../privateProject
}


#android{
    #DEFINES += LIBS_SUFFIX='\\"_$${QT_ARCH}.so\\"'
    #QT += androidextras
#}


#Add sources for open other open source LGPL libraries
#OPTIONAL  LPGL EXTERNAL LIBRARIES
exists($$PWD/publicLibs/publicLibs.pri) {
   include($$PWD/publicLibs/publicLibs.pri)
} else{
    exists($$PWD/../publicLibs/publicLibs.pri) {
      include($$PWD/../publicLibs/publicLibs.pri)
    }
}

PLATFORMS_DIR = $$PWD/platforms
android: {
    CONFIG += rtti
    ANDROID_PACKAGE_SOURCE_DIR = $$PLATFORMS_DIR/android
    INCLUDEPATH += $$ANDROID_PACKAGE_SOURCE_DIR
    OTHER_FILES += $$ANDROID_PACKAGE_SOURCE_DIR/QShareUtils.java

    DISTFILES += \
        $$ANDROID_PACKAGE_SOURCE_DIR/AndroidManifest.xml \
        $$ANDROID_PACKAGE_SOURCE_DIR/build.gradle \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradle.properties \
        $$ANDROID_PACKAGE_SOURCE_DIR/local.properties \
        $$ANDROID_PACKAGE_SOURCE_DIR/google-services.json \
        $$ANDROID_PACKAGE_SOURCE_DIR/QShareUtils.java \
        $$ANDROID_PACKAGE_SOURCE_DIR/res/values/apptheme.xml \
        $$ANDROID_PACKAGE_SOURCE_DIR/res/values/strings.xml \
        $$ANDROID_PACKAGE_SOURCE_DIR/res/drawable/splash.xml \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradlew \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradlew.bat \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradle/wrapper/gradle-wrapper.jar \
        $$ANDROID_PACKAGE_SOURCE_DIR/gradle/wrapper/gradle-wrapper.properties
}

ios: {
    CONFIG -= bitcode
#    ios_icon.files = $$files($$PLATFORMS_DIR/ios/icons/AppIcon*.png)
#    QMAKE_BUNDLE_DATA += ios_icon

#    itunes_icon.files = $$files($$PLATFORMS_DIR/ios/iTunesArtwork*)
#    QMAKE_BUNDLE_DATA += itunes_icon

    QMAKE_ASSET_CATALOGS = $$PLATFORMS_DIR/ios/Images.xcassets
    QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"

    app_launch_screen.files = $$files($$PLATFORMS_DIR/ios/RighthereLaunchScreen.storyboard)
    QMAKE_BUNDLE_DATA += app_launch_screen

    app_launch_images.files = $$files($$PLATFORMS_DIR/ios/launchscreen/LaunchImage*.png)
    QMAKE_BUNDLE_DATA += app_launch_images
    QMAKE_INFO_PLIST = $$PLATFORMS_DIR/ios/Info.plist

    DISTFILES += \
        $$PLATFORMS_DIR/ios/Info.plist \
        $$PLATFORMS_DIR/ios/GoogleService-Info.plist \
        $$PLATFORMS_DIR/ios/App.entitlements \
        $$PLATFORMS_DIR/ios/RighthereLaunchScreen.storyboard

    # You must deploy your Google Play config file
    deployment.files = $$PLATFORMS_DIR/ios/GoogleService-Info.plist
    deployment.path =
    QMAKE_BUNDLE_DATA += deployment
    #QMAKE_TARGET_BUNDLE_PREFIX = righthere
    QMAKE_BUNDLE = righthere #cutspotui
#    Q_ENABLE_BITCODE.name = ENABLE_BITCODE
#    Q_ENABLE_BITCODE.value = NO
#    QMAKE_MAC_XCODE_SETTINGS += Q_ENABLE_BITCODE

    APP_ENTITLEMENTS.name = CODE_SIGN_ENTITLEMENTS
    APP_ENTITLEMENTS.value = $$PLATFORMS_DIR/ios/App.entitlements
    QMAKE_MAC_XCODE_SETTINGS += APP_ENTITLEMENTS
    OTHER_FILES += $$PLATFORMS_DIR/ios/RighthereLaunchScreen.storyboard
    LIBS += -framework EventKit
}


#END ADD OPTIONAL  LPGL EXTERNAL LIBRARIES

#END PRIVATE PROJECT

#END MANDATORY












